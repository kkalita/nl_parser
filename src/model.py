import sys
import json
import itertools as tools
import numpy as np
from parse_tree import ParseTree


class GrammarModel:
    """Model for PCFG-LA"""
    _NT = 'NONTERMINAL'
    _UNARY = 'UNARYRULE'
    _BINARY = 'BINARYRULE'
    EM_ITERATIONS = 20
    def __init__(self, ruleCountFile):
        self.rules_prob = {}
        self.LA = {}
        self.la_efficiency = {}
        nt_prob = {}
        #  read rules probabilities for a baseline grammar
        for line in open(ruleCountFile):
            count, kind, A, B, C = line.split()
            count = int(count)
            if kind == GrammarModel._NT:
                nt_prob[A] = count
                self.LA[A] = [A + '++0']
            else:
                if kind == GrammarModel._UNARY:
                    self.rules_prob[(A + '++0', B, '-')] = count / nt_prob[A]
                elif kind == GrammarModel._BINARY:
                    self.rules_prob[(A + '++0', B + '++0', C + '++0')] = count / nt_prob[A]
        self.nts = nt_prob.keys()

    def rules(self):
        return self.rules_prob.keys()

    def train(self, trainingFile, splits=2):
        with open(trainingFile) as _file:
            lines = _file.readlines()
        trees = [ParseTree(json.loads(l)) for l in lines]
        f = {}
        for i in range(splits):
            self._split_grammar()
            for k in range(GrammarModel.EM_ITERATIONS):
                sys.stderr.write(str(k) + '\n')
                self._update_rules_probs(trees, f)
            self._merge_grammar(trees)

    def _split_grammar(self):
        new_rules_prob = {}
        new_LA = {}
        splitted = {}
        for A in self.nts:
            new_LA[A] = []
            for Ax in self.LA[A]:
                new_LA[A].append(Ax + '0')
                new_LA[A].append(Ax + '1')
                self.la_efficiency[Ax + '0', Ax + '1'] = 1
                splitted[Ax] = (Ax + '0', Ax + '1')
        for ((A, B, C), prob) in self.rules_prob.items():
            for Ax in splitted[A]:
                if C == '-':
                    new_rules_prob[Ax, B, C] = prob
                else:
                    n = len(splitted[B]) * len(splitted[C])
                    p = prob * 0.99 / n
                    mod = prob * 0.01 / n
                    dist = np.random.dirichlet(np.ones(n), size=1)[0]
                    for i, (By, Cz) in enumerate(tools.product(splitted[B], splitted[C])):
                        new_rules_prob[Ax, By, Cz] = p + mod * dist[i]

        self.rules_prob = new_rules_prob
        self.LA = new_LA

    def base_symbol(self, A):
        end = A.find('++')
        if end >= 0:
            A = A[:end]
            return A

    def _safe_add(self, _dict, key, val):
        if key not in _dict:
            _dict[key] = val
        else:
            _dict[key] += val

    def _merge_grammar(self, trees):
        freq = {}
        for (Ax, By, Cz), p in self.rules_prob.items():
            if Cz != '-':
                self._safe_add(freq, By, p)
                self._safe_add(freq, Cz, p)
        (to_merge, rev_merge) = self._count_la_efficiency(trees, freq)
        self._merge_rules(to_merge, rev_merge, freq)
        new_LA = {N: [] for N in self.nts}
        for A in self.LA:
            for i in range(len(self.LA[A])):
                if self.LA[A][i] not in to_merge:
                    new_LA[A].append(self.LA[A][i])
        self.LA = new_LA

    def _merge_rules(self, to_merge, rev_merge, freq):
        new_rules_prob = {}
        for (A, B, C), p in self.rules_prob.items():
            if B in to_merge or C in to_merge:
                By = to_merge[B] if B in to_merge else B
                Cz = to_merge[C] if C in to_merge else C
                self._safe_add(new_rules_prob, (A, By, Cz), self.rules_prob[A, B, C])
            else:
                self._safe_add(new_rules_prob, (A, B, C), self.rules_prob[A, B, C])
        self.rules_prob = {}
        for (A, B, C), p in new_rules_prob.items():
            if A in to_merge:
                Ax = to_merge[A]
                mod = freq[A] / (freq[A] + freq[Ax])
                self._safe_add(self.rules_prob, (Ax, B, C), mod * new_rules_prob[A, B, C])
            else:
                mod = 1
                if A in rev_merge:
                    Ay = rev_merge[A]
                    mod = freq[A] / (freq[A] + freq[Ay])
                self._safe_add(self.rules_prob, (A, B, C), mod * new_rules_prob[A, B, C])

    def _count_la_efficiency(self, trees, freq):
        for t in trees:
            _in = t.count_inside(self)
            out = t.count_outside(self, _in)
            for node in t.nodes:
                N, span = node[0], t.get_span(node)
                for Ax, Ay in tools.product(self.LA[N], repeat=2):
                    if (Ax, Ay) in self.la_efficiency:
                        tree_prob = sum(_in[X, span] * out[X, span] for X in (Ax, Ay))
                        if tree_prob > 0:
                            p_x = freq[Ax] / (freq[Ax] + freq[Ay])
                            p_y = freq[Ay] / (freq[Ax] + freq[Ay])
                            node_in = p_x * _in[Ax, span] + p_y * _in[Ay, span]
                            node_out = out[Ax, span] + out[Ay, span]
                            self.la_efficiency[Ax, Ay] *= node_in * node_out / tree_prob

        xs = [(i, j) for (i, j) in self.la_efficiency.items()]
        xs.sort(key=lambda x: x[1], reverse=True)
        to_merge = {}
        rev_merge = {}
        for (Ax, Ay), p in xs[:(2 * len(xs)) // 3]:
            to_merge[Ay] = Ax
            rev_merge[Ax] = Ay
        return to_merge, rev_merge

    def _update_rules_probs(self, trees, f):
        for rule in self.rules():
                f[rule] = 0
        for tree in trees:
            count = self._count_rules(tree)
            for rule in count.keys():
                    f[rule] += count[rule]
        sum_prob = {}
        for A, B, C in self.rules():
            self._safe_add(sum_prob, A, f[A, B, C])
        for A, B, C in self.rules():
            self.rules_prob[A, B, C] = f[A, B, C] / sum_prob[A]

    def _count_rules(self, tree):
        count = {}
        Z, mi = self._io_prob(tree)
        for (rule, span), val in mi.items():
            self._safe_add(count, rule, val)
        for rule in count.keys():
            count[rule] /= Z
        return count

    def _io_prob(self, tree):
        _in = tree.count_inside(self)
        out = tree.count_outside(self, _in)
        n = len(tree.sentence())
        Z = sum(_in[root, (0, n - 1)] for root in self.LA[tree.root_symbol()])
        mi = tree.count_mi(self, _in, out)
        return Z, mi

