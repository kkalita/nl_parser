import itertools as tools


class ParseTree:
    def __init__(self, tree):
        self._tree = tree
        self._sentence = []
        self.nodes = []
        self._build_sentence(tree, self._sentence)
        self._add_span(self._tree)

    def _add_span(self, node, i=0):
        if len(node) == 2:
            node.append((i, i))
            return (i, i)
        a, b = self._add_span(node[1], i)
        c, d = self._add_span(node[2], b+1)
        node.append((a, d))
        return (a, d)

    def _build_sentence(self, node, sentence):
        self.nodes.append(node)
        if len(node) == 2:
            sentence.append(node[1])
            return
        for i in range(1, len(node)):
            self._build_sentence(node[i], sentence)

    def get_span(self, node):
        return node[-1]

    def root_symbol(self):
        return self._tree[0]

    def sentence(self):
        return self._sentence

    def count_inside(self, model):
        def dfs(node, _in):
            if len(node) == 3:
                A, w, span = node
                for Ax in model.LA[A]:
                    _in[Ax, span] = model.rules_prob[Ax, w, '-']
                return
            dfs(node[1], _in)
            dfs(node[2], _in)
            A, B, C = node[0], node[1][0], node[2][0]
            span_A, span_B, span_C = node[3], self.get_span(node[1]), self.get_span(node[2])
            for Ax in model.LA[A]:
                _in[Ax, span_A] = 0
                for By, Cz in tools.product(model.LA[B], model.LA[C]):
                    _in[Ax, span_A] += _in[By, span_B] * _in[Cz, span_C] * model.rules_prob[Ax, By, Cz]
        _in = {}
        dfs(self._tree, _in)
        return _in

    def count_outside(self, model, _in):
        def dfs(node, out):
            if len(node) == 3:
                return
            A, B, C = node[0], node[1][0], node[2][0]
            span_A, span_B, span_C = node[3], self.get_span(node[1]), self.get_span(node[2])
            for By in model.LA[B]:
                out[By, span_B] = 0
                for Ax, Cz in tools.product(model.LA[A], model.LA[C]):
                    out[By, span_B] += out[Ax, span_A] * _in[Cz, span_C] * model.rules_prob[Ax, By, Cz]
            for Cz in model.LA[C]:
                out[Cz, span_C] = 0
                for Ax, By in tools.product(model.LA[A], model.LA[B]):
                    out[Cz, span_C] += out[Ax, span_A] * _in[By, span_B] * model.rules_prob[Ax, By, Cz]
            dfs(node[1], out)
            dfs(node[2], out)
        root, span = self._tree[0], self._tree[3]
        out = {(A, span): 1 / len(model.LA[root]) for A in model.LA[root]}
        dfs(self._tree, out)
        return out

    def count_mi(self, model, _in, out):
        def dfs(node, mi):
            if len(node) == 3:
                A, w, span = node
                for Ax in model.LA[A]:
                    rule = Ax, w, '-'
                    mi[rule, span] = _in[Ax, span] * out[Ax, span]
                return
            A, B, C = node[0], node[1][0], node[2][0]
            span_A, span_B, span_C = node[3], self.get_span(node[1]), self.get_span(node[2])
            for Ax, By, Cz in tools.product(model.LA[A], model.LA[B], model.LA[C]):
                rule = Ax, By, Cz
                mi[rule, span_A] = model.rules_prob[rule] * _in[By, span_B] * _in[Cz, span_C] * out[Ax, span_A]
            dfs(node[1], mi)
            dfs(node[2], mi)
        mi = {}
        dfs(self._tree, mi)
        return mi
