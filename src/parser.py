import sys
import json
import numpy as np
import itertools as tools
import re
from model import GrammarModel

_RARE_ = '_RARE_'
_NUMBER_ = '_NUMBER_'
_NOUN_ = '_NOUN_'


class CKYParser:
    def __init__(self, model, rep=None):
        self.unary_rules = {}
        self.binary_rules = {}
        self.non_terminals = set()
        self.rep = rep
        self.model = model
        for (A, B, C), p in model.rules_prob.items():
            if C == '-':
                if A in self.unary_rules:
                    self.unary_rules[A][B] = p
                else:
                    self.unary_rules[A] = {B: p}
            else:
                if A not in self.binary_rules:
                    self.binary_rules[A] = {B: {C: p}}
                elif B not in self.binary_rules[A]:
                    self.binary_rules[A][B] = {C: p}
                elif C not in self.binary_rules[A][B]:
                    self.binary_rules[A][B][C] = p
            self.non_terminals.add(A)

    def _make_tree(self, tree, last, bp_table, s, pi_table):
        tree[0] = self.model.base_symbol(tree[0])
        Y, Z, i, k, j = last
        if k == -1:
            tree.append(s[i])
            return
        tree.append([Y])
        tree.append([Z])
        B, C, l = bp_table[Y][i][k]
        self._make_tree(tree[1], (B, C, i, l, k), bp_table, s, pi_table)
        B, C, l = bp_table[Z][k+1][j]
        self._make_tree(tree[2], (B, C, k+1, l, j), bp_table, s, pi_table)
        return tree

    def _cky_init(self, s, n):
        N = self.non_terminals
        pi_table = {key: np.zeros((n, n)) for key in N}
        bp_table = {key: [[("", "", -1) for x in range(n)] for y in range(n)] for key in N}
        for i in range(n):
            is_rare = True
            for nt in self.unary_rules.keys():
                if s[i] in self.unary_rules[nt]:
                    pi_table[nt][i][i] = self.unary_rules[nt][s[i]]
                    is_rare = False

            if is_rare:
                if re.search('\d', s[i]) is not None:
                    kind = _NUMBER_
                elif i > 0 and s[i][0].isupper():
                    kind = _NOUN_
                else:
                    kind = _RARE_
                for nt in self.unary_rules.keys():
                    if kind in self.unary_rules[nt]:
                        pi_table[nt][i][i] = self.unary_rules[nt][kind]
        return pi_table, bp_table

    def preprocess(self, sentence):
        s = sentence.split()
        n = len(s)
        pi_table, bp_table = self._cky_init(s, n)
        self.rep = {(i, j): [] for i, j in tools.product(range(n), repeat=2)}
        for diag in range(1, n):
            for i in range(n - diag):
                j = i + diag
                for nt in self.binary_rules.keys():
                    p, b = self._find_rep(nt, i, j, pi_table)
                    pi_table[nt][i][j], bp_table[nt][i][j] = p, b

    def _find_rep(self, X, i, j, pi_table):
        prob = 0
        bp = ("", "", 0)
        m = self.model
        for k in range(i, j):
            for Y in self.binary_rules[X]:
                for Z in self.binary_rules[X][Y]:
                    if self.binary_rules[X][Y][Z] * pi_table[Y][i][k] * pi_table[Z][k+1][j] > 0:
                        A, B, C = m.base_symbol(X), m.base_symbol(Y), m.base_symbol(Z)
                        self.rep[i, j].append((A, B, C, k))
                    if self.binary_rules[X][Y][Z] * pi_table[Y][i][k] * pi_table[Z][k+1][j] > prob:
                        prob = self.binary_rules[X][Y][Z] * pi_table[Y][i][k] * pi_table[Z][k+1][j]
                        bp = (Y, Z, k)
        return (prob, bp)

    def parse(self, sentence):
        s = sentence.split()
        n = len(s)
        LA = self.model.LA
        pi_table, bp_table = self._cky_init(s, n)
        for diag in range(1, n):
            for i in range(n - diag):
                j = i + diag
                for (A, B, C, k) in self.rep[i, j]:
                    for Ax, By, Cz in tools.product(LA[A], LA[B], LA[C]):
                        prob = self.binary_rules[Ax][By][Cz] * pi_table[By][i][k] * pi_table[Cz][k+1][j]
                        if prob > pi_table[Ax][i][j]:
                            pi_table[Ax][i][j], bp_table[Ax][i][j] = prob, (By, Cz, k)

        return self._find_max_tree(pi_table, bp_table, s)

    def _find_max_tree(self, pi_table, bp_table, s):
        n = len(s)
        _max = 0
        root = ""
        for A in self.model.LA['SBARQ']:
            if pi_table[A][0][n-1] > _max:
                _max = pi_table[A][0][n-1]
                root = A
        Y, Z, k = bp_table[root][0][n - 1]
        return self._make_tree([root], (Y, Z, 0, k, n - 1), bp_table, s, pi_table)
